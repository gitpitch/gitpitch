[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/gitpitch/gitpitch/master?grs=bitbucket)

#### WEBSITE: [www.gitpitch.com](https://gitpitch.com) | HOW-TO : [GitPitch Wiki](https://github.com/gitpitch/gitpitch/wiki) | TWITTER: [@gitpitch](https://twitter.com/gitpitch)

# GitPitch - Hello, World!
A simple slideshow presentation to introduce the GitPitch service.

View the `raw` **PITCHME.md** markdown to see how each slide in the presentation has been implemented.
